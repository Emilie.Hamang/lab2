package INF101.lab2.pokemon;

import java.util.Random;

public class Pokemon {
    ////// Oppgave 1a
    // Create field variables here:
    String name;
    int healthPoints;
    int maxHealthPoints;
    int strength;

    ///// Oppgave 1b
    // Create a constructor here:
    Pokemon(String name, int healthPoints, int strength) {
        this.name = name;
        this.maxHealthPoints = healthPoints;
        this.healthPoints = healthPoints;
        this.strength = strength;
    }

    ///// Oppgave 2
    /**
     * Get name of the pokémon
     * 
     * @return name of pokémon
     */
    String getName() {
        return name;
    }

    /**
     * Get strength of the pokémon
     * 
     * @return strength of pokémon
     */
    int getStrength() {
        return strength;
    }

    /**
     * Get current health points of pokémon
     * 
     * @return current HP of pokémon
     */
    int getCurrentHP() {
        return healthPoints;
    }

    /**
     * Get maximum health points of pokémon
     * 
     * @return max HP of pokémon
     */
    int getMaxHP() {
        return maxHealthPoints;
    }

    /**
     * Check if the pokémon is alive.
     * A pokemon is alive if current HP is higher than 0
     * 
     * @return true if current HP > 0, false if not
     */
    boolean isAlive() {
        return getCurrentHP() > 0;
    }

    ///// Oppgave 4
    /**
     * Damage the pokémon. This method reduces the number of
     * health points the pokémon has by <code>damageTaken</code>.
     * If <code>damageTaken</code> is higher than the number of current
     * health points then set current HP to 0.
     *
     * It should not be possible to deal negative damage, i.e. increase the number
     * of health points.
     *
     * The method should print how much HP the pokemon is left with.
     *
     * @param damageTaken
     */
    void damage(int damageTaken) {
        if (damageTaken < 0) {
            damageTaken = 0;
        }
        this.healthPoints -= damageTaken;
        if (this.healthPoints < 0) {
            this.healthPoints = 0;
            System.out
                    .println(this.name + " takes " + damageTaken + " damage and is left with " + this.healthPoints + "/"
                            + this.maxHealthPoints + " HP");
        } else {
            System.out
                    .println(this.name + " takes " + damageTaken + " damage and is left with " + this.healthPoints + "/"
                            + this.maxHealthPoints + " HP");
        }
    }

    ///// Oppgave 5
    /**
     * Attack another pokémon. The method conducts an attack by <code>this</code>
     * on <code>target</code>. Calculate the damage using the pokémons strength
     * and a random element. Reduce <code>target</code>s health.
     * 
     * If <code>target</code> has 0 HP then print that it was defeated.
     * 
     * @param target pokémon that is being attacked
     */
    void attack(Pokemon target) {
        if (!this.isAlive()) {
            return;
        }
        // prints out the attack
        System.out.println(this.name + " attacks " + target.name + ".");
        // find a random number
        Random randInt = new Random();
        int damageInflicted = randInt.nextInt(this.strength) + 1;

        // damage on one of the pokemons
        target.damage(damageInflicted);
        if (target.healthPoints <= 0) {
            System.out.println(target.name + " is defeated by " + this.name + ".");
        }
    }

    ///// Oppgave 3
    @Override
    public String toString() {
        String health = Integer.toString(this.healthPoints);
        String healthMax = Integer.toString(this.maxHealthPoints);
        String str = Integer.toString(this.strength);
        return (this.name + " HP: (" + health + "/" + healthMax + ") " + "STR: " + str);
    }
}
